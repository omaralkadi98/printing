/// InvoiceID : 1
/// DateGr : "2020/01/03"
/// NetInv : 1000

class ReturnInvoiceNumber {
  ReturnInvoiceNumber({
    this.invoiceID,
    this.dateGr,
    this.netInv,
  });

  ReturnInvoiceNumber.fromJson(dynamic json) {
    invoiceID = json['InvoiceID'];
    dateGr = json['DateGr'];
    netInv = json['NetInv'];
  }

  int? invoiceID;
  String? dateGr;
  double? netInv;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['InvoiceID'] = invoiceID;
    map['DateGr'] = dateGr;
    map['NetInv'] = netInv;
    return map;
  }
}
