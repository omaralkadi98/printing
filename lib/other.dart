import 'package:flutter/cupertino.dart';

bool isArabic(context) {
  if (context != null) {
    final Locale appLocale = Localizations.localeOf(context);
    if (appLocale.languageCode != 'ar') return false;
  }
  return true;
}
