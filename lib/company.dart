class Company {
  String? nameArb;
  String? nameEng;
  String? addressArb;
  String? addressEng;
  String? emails;
  String? cIVATNo;
  String? phone;
  String? fax;
  String? cIImage;

  Company(
      {this.nameArb,
      this.nameEng,
      this.addressArb,
      this.addressEng,
      this.emails,
      this.cIVATNo,
      this.phone,
      this.fax,
      this.cIImage});

  Company.fromJson(Map<String, dynamic> json) {
    nameArb = json['NameArb'];
    nameEng = json['NameEng'];
    addressArb = json['AddressArb'];
    addressEng = json['AddressEng'];
    emails = json['Emails'];
    cIVATNo = json['CIVATNo'];
    phone = json['Phone'];
    fax = json['Fax'];
    cIImage = json['CIImage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['NameArb'] = this.nameArb;
    data['NameEng'] = this.nameEng;
    data['AddressArb'] = this.addressArb;
    data['AddressEng'] = this.addressEng;
    data['Emails'] = this.emails;
    data['CIVATNo'] = this.cIVATNo;
    data['Phone'] = this.phone;
    data['Fax'] = this.fax;
    data['CIImage'] = this.cIImage;
    return data;
  }
}
