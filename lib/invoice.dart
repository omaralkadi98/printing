import 'package:inventor_for_printing/return_invoice_number.dart';

/// InvoiceCode : 4297
/// InvoiceID : 1900
/// DateGr : "2020/03/03"
/// TypeInvoiceArb : "kb"
/// TypeInvoiceEng : "Cash"
/// TimeIN : "22:14"
/// PCNameArb : "erer"
/// PCNameEng : "ertyui"
/// UserName : "ijij"
/// TotalInv : 2500
/// DiscountInv : 0
/// TaxAmount : 125
/// NetInv : 2625
/// InvoiceDetails : [{"InvoiceCode":4297,"ItemID":"400082","ItemNameArb":"ksjdf","ItemNameEng":"sdfsdf","UnitArb":"asdasd","UnitEng":"bac","Quantity":1,"Price":2000,"TotalDetails":2000,"DiscountDetails":0,"TaxDetails":100,"PerTaxAmount":0,"NetDetails":2100,"NotesItem":""},null]

class Invoice {
  Invoice({
    this.invoiceCode,
    this.invoiceID,
    this.returnInvoiceID,
    this.dateGr,
    this.typeInvoiceArb,
    this.typeInvoiceEng,
    this.timeIN,
    this.pCNameArb,
    this.pCNameEng,
    this.userName,
    this.totalInv,
    this.discountInv,
    this.taxAmount,
    this.netInv,
    this.invoiceDetails,
    this.pCTaxNo,
    this.invNo,
    this.rInvNo,
    this.typeDocArb,
    this.typeDocEng,
    this.prCuID,
  });

  // String get typeInvoice {
  //   return isArabic
  //       ? typeInvoiceArb ?? '$typeInvoiceEng'
  //       : typeInvoiceEng ?? '$typeInvoiceArb';
  // }

  // String get typeDoc {
  //   return isArabic ? typeDocArb ?? '$typeDocEng' : typeDocEng ?? '$typeDocArb';
  // }

  // String get pCName {
  //   return isArabic ? pCNameArb ?? '$pCNameEng' : pCNameEng ?? '$pCNameArb';
  // }

  Invoice.fromJson(dynamic json) {
    invoiceCode = json['InvoiceCode'];
    invoiceID = json['InvoiceID'];
    returnInvoiceID = json['ReturnInvoiceID'];
    dateGr = json['DateGr'];
    typeInvoiceArb = json['TypeInvoiceArb'];
    typeInvoiceEng = json['TypeInvoiceEng'];
    timeIN = json['TimeIN'];
    pCNameArb = json['PCNameArb'];
    pCNameEng = json['PCNameEng'];
    userName = json['UserName'];
    totalInv = json['TotalInv'];
    discountInv = json['DiscountInv'];
    taxAmount = json['TaxAmount'];
    netInv = json['NetInv'];
    invNo = json['InvNo'];
    rInvNo = json['RInvNo'];
    typeDocArb = json['TypeDocArb'];
    typeDocEng = json['TypeDocEng'];
    pCTaxNo = json['PCTaxNo'];
    prCuID = json['PrCuID'];
    if (json['InvoiceDetails'] != null) {
      invoiceDetails = [];
      json['InvoiceDetails'].forEach((v) {
        invoiceDetails?.add(InvoiceDetail.fromJson(v));
      });
    }
  }

  int? invoiceCode;
  int? invoiceID;
  int? returnInvoiceID;
  String? dateGr;
  String? typeInvoiceArb;
  String? typeInvoiceEng;
  String? timeIN;
  String? pCNameArb;
  String? pCNameEng;
  String? userName;
  String? pCTaxNo;
  String? typeDocEng;
  String? typeDocArb;
  String? rInvNo;
  String? invNo;
  double? totalInv;
  double? discountInv;
  double? taxAmount;
  double? netInv;
  int? prCuID;
  List<InvoiceDetail>? invoiceDetails;

  ReturnInvoiceNumber? _returnInvoiceNumber;

  int get quantity {
    int sum = 0;
    for (InvoiceDetail detail in invoiceDetails!)
      sum += detail.quantity!.toInt();
    return sum;
  }

  get taxPercint => taxAmount! / totalAfterDiscount * 100;

  double get totalAfterDiscount => totalInv! - discountInv!;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['InvoiceCode'] = invoiceCode;
    map['InvoiceID'] = invoiceID;
    map['ReturnInvoiceID'] = returnInvoiceID;
    map['DateGr'] = dateGr;
    map['TypeInvoiceArb'] = typeInvoiceArb;
    map['TypeInvoiceEng'] = typeInvoiceEng;
    map['TimeIN'] = timeIN;
    map['PCNameArb'] = pCNameArb;
    map['PCNameEng'] = pCNameEng;
    map['UserName'] = userName;
    map['TotalInv'] = totalInv;
    map['DiscountInv'] = discountInv;
    map['TaxAmount'] = taxAmount;
    map['PCTaxNo'] = pCTaxNo;
    map['typeDocEng'] = typeDocEng;
    map['typeDocArb'] = typeDocArb;
    map['rInvNo'] = rInvNo;
    map['invNo'] = invNo;
    map['NetInv'] = netInv;
    map['PrCuID'] = prCuID;
    if (invoiceDetails != null) {
      map['InvoiceDetails'] = invoiceDetails?.map((v) => v.toJson()).toList();
    }
    return map;
  }

  ReturnInvoiceNumber? get returnInvoiceNumber => _returnInvoiceNumber;

  String? qrCode = 'mynhtbgrvfcd tbgrvf ntbgrvfd';

  set returnInvoiceNumber(invoiceNumber) =>
      _returnInvoiceNumber = invoiceNumber;
}

/// InvoiceCode : 4297
/// ItemID : "400082"
/// ItemNameArb : "ksjdf"
/// ItemNameEng : "sdfsdf"
/// UnitArb : "asdasd"
/// UnitEng : "bac"
/// Quantity : 1
/// Price : 2000
/// TotalDetails : 2000
/// DiscountDetails : 0
/// TaxDetails : 100
/// PerTaxAmount : 0
/// NetDetails : 2100
/// NotesItem : ""

class InvoiceDetail {
  InvoiceDetail({
    this.invoiceCode,
    this.itemID,
    this.itemNameArb,
    this.itemNameEng,
    this.unitArb,
    this.unitEng,
    this.quantity,
    this.price,
    this.totalDetails,
    this.discountDetails,
    this.taxDetails,
    this.perTaxAmount,
    this.netDetails,
    this.notesItem,
    this.unitId,
    this.warehouseId,
    this.quantityFull,
    this.costPrice,
  });

  InvoiceDetail.fromJson(dynamic json) {
    invoiceCode = json['InvoiceCode'];
    itemID = json['ItemID'];
    itemNameArb = json['ItemNameArb'];
    itemNameEng = json['ItemNameEng'];
    unitArb = json['UnitArb'];
    unitEng = json['UnitEng'];
    quantity = json['Quantity'];
    price = json['Price'];
    totalDetails = json['TotalDetails'];
    discountDetails = json['DiscountDetails'];
    taxDetails = json['TaxDetails'];
    perTaxAmount = json['PerTaxAmount'];
    netDetails = json['NetDetails'];
    notesItem = json['NotesItem'];
    unitId = json['UnitID'];
    warehouseId = json['WarehouseID'];
    quantityFull = json['QuantityFull'];
    costPrice = json['CostPrice'];
  }

  // String get unitName {
  //   return isArabic ? unitArb ?? '$unitEng' : unitEng ?? '$unitArb';
  // }

  int? invoiceCode;
  String? itemID;
  int? unitId;
  String? itemNameArb;
  String? itemNameEng;
  String? unitArb;
  String? unitEng;
  double? quantity;
  double? price;
  double? totalDetails;
  double? discountDetails;
  double? taxDetails;
  double? perTaxAmount;
  double? netDetails;
  String? notesItem;
  int? warehouseId;
  double? quantityFull;
  double? costPrice;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['InvoiceCode'] = invoiceCode;
    map['ItemID'] = itemID;
    map['ItemNameArb'] = itemNameArb;
    map['ItemNameEng'] = itemNameEng;
    map['UnitArb'] = unitArb;
    map['UnitEng'] = unitEng;
    map['Quantity'] = quantity;
    map['Price'] = price;
    map['TotalDetails'] = totalDetails;
    map['DiscountDetails'] = discountDetails;
    map['TaxDetails'] = taxDetails;
    map['PerTaxAmount'] = perTaxAmount;
    map['NetDetails'] = netDetails;
    map['NotesItem'] = notesItem;
    map['UnitID'] = unitId;
    map['WarehouseID'] = warehouseId;
    map['QuantityFull'] = quantityFull;
    map['CostPrice'] = costPrice;
    return map;
  }

  double get taxAfterDiscount => totalAfterDiscount * taxPercent / 100;

  double get totalAfterDiscountAndTax => totalAfterDiscount + taxAfterDiscount;

  double get totalAfterDiscount => totalBeforeDiscountAndTax - discountDetails!;

  double get totalBeforeDiscountAndTax => quantity! * price!;

  //todo
  double get taxPercent => 15;
}
